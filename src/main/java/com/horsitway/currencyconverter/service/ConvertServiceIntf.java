package com.horsitway.currencyconverter.service;

import org.json.JSONObject;

public interface ConvertServiceIntf {
	
	public void update(); //Update tables and save update time
	public JSONObject getRate(String table); //Download current exchange rates from selected NBP table
	public double convert(String oldCurrency, String newCurrency, double value);
}
