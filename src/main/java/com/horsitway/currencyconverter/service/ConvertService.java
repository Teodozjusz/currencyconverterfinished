package com.horsitway.currencyconverter.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ConvertService implements ConvertServiceIntf{

	JSONObject tableA;
	JSONObject tableB;
	
	Calendar downloadDate;
	ConvertService () {
		update();
	}
	
	@Override
	public void update() { //Update tables and save update time
		tableA = getRate("A");
		tableB = getRate("B");
		downloadDate = Calendar.getInstance();
	}
	
	@Override
	public JSONObject getRate(String table) { //Download current exchange rates from selected NBP table
		JSONObject json = new JSONObject();
		try {
			URL url = new URL("http://api.nbp.pl/api/exchangerates/tables/" + table + "/");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			json = new JSONArray(response.toString()).getJSONObject(0);
			
		}
		catch (Exception e) {
			json.append("error", e.toString());
		}
		return json;
	}
	
	@Override
	public double convert(String oldCurrency, String newCurrency, double value) {
		
		if(downloadDate.get(Calendar.DAY_OF_WEEK) != Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
			update(); //Update if not updated today
		}
		
		double originMid;
		double targetMid;
		if (!newCurrency.equals("PLN")) {
			targetMid = findMid(tableA, newCurrency);
			if(targetMid == -1) targetMid = findMid(tableB, newCurrency);
		}
		else {
			targetMid = 1;
		}
		
		if(!oldCurrency.equals("PLN")) {			  //Verify if conversion is from PLN
			originMid = findMid(tableA, oldCurrency); //If not, find appropriate exchange rate
			if(originMid == -1) originMid = findMid(tableB, oldCurrency);
		}
		else {
			originMid = 1;
		}
		if (originMid == -1) return -1;
		else if (targetMid == -1) return -2;
		else return (value * originMid) / targetMid;
		
	}
	
	public double findMid(JSONObject table, String currency) { //Find given currency in selected table
		double mid = -1; //Currency with given code not found
		
		for (int i = 0; i < table.getJSONArray("rates").length() ; i++) {
			if(table.getJSONArray("rates").getJSONObject(i).getString("code").equals(currency)) {
				mid = table.getJSONArray("rates").getJSONObject(i).getDouble("mid");
			}
		}
		return mid;
	}
}
