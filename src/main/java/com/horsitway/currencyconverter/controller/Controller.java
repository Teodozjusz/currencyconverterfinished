package com.horsitway.currencyconverter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.horsitway.currencyconverter.service.ConvertService;

@RestController
public class Controller {
	
	@Autowired
	ConvertService service;
	
	@RequestMapping(method = RequestMethod.GET, value = "/convert")
	public double convert(@RequestParam("from") String oldCurrency, 
			@RequestParam("to") String newCurrency,
			@RequestParam("v") double value) {
		return service.convert(oldCurrency, newCurrency, value); 				  			 
	}
}

//Error codes: -1 - Currency to convert from not found
//			   -2 - Currency to convert to not found